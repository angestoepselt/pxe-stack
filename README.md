# deploy infra

[![Build Status](https://drone.z31.it/api/badges/angestoepselt/pxe-stack/status.svg)](https://drone.z31.it/angestoepselt/pxe-stack)

## Info
Lokale Installation mit Docker um PC und Laptop aufzusetzen

Container die gestartet werden:
- apt-cacher-ng
- nginx - statische Inhalte
- tftp Server
- git

## Verwendung:

Klone das Repo auf einem Dockerhost aus und führe ``docker compose up -d`` aus. In deinem DHCP Server muss du dann noch PXE / Netboot auf das Verzeichnis legacy/pxelinux.cfg einstellen. Der Server ist die IP deines Dockerhost


## Todo

- Erstelle ein Installationskript um das installieren so einfach wie möglich zu machen
- Stelle das install.sh Skript unter einer Subdomain zur Verfügung