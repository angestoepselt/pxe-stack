#!/bin/sh

if [ "$(ls -A /git)" ]; then
    cd /git
    git pull
else
    git clone ${GIT_REPO_URL} /git
fi

echo "finished, exit now"